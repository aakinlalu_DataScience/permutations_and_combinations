# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* There are three functions in the permutations_and_combinations module:
     1. permutation(n, r)
         *  n stands for the objects to be arranged
         *  r stands for how many time the objects should be arranged
     2. factorial_multiplication(r)
     3. combination(n, r)
         *  n is the total number of items and 
         *  r is the number in each selection
* Version 1.0
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions
    * from permutations_and_combinations import permutation, factorial_multiplication, combination
    * print permutation(5, 2)
    * print combination(5, 2)

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact