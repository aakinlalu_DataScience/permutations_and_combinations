# -*- coding: utf-8 -*-
"""
Created on Mon May 25 11:39:28 2015

@author: adebayoakinlalu
"""

def permutation(n, r):
    ''' 
    n stands for the objects to be arranged
    r stands for how many time the objects shoud be arranged
    pem(6, 3) => 120
    pem(8, 2) => 56
    '''
    permutation =  1
    for i in range(r):
        permutation = permutation * (n - i)
    return permutation
    
def factorial_multiplication(r):
    '''
    r will be multiplied by r - 1 until r eaqul to 1
    factorial_multiplication(5) => 120
    '''
    if r == 1:
        return r
    else:
        return r * factorial_multiplication(r - 1)
        

def combination(n, r):
    '''
    The number of possible combinations denoted by nCr,
    where n is the total number of items and r is the 
    number in each selection.
    for example: combination(5, 2) => 10 and
    combination(24, 4) => 10626
    '''
    return permutation(n, r)/factorial_multiplication(r)
    
        
    
    